import {createServer} from 'http';
import {existsSync, createReadStream} from 'fs';
import os from 'os';
import {Cipher} from './webcrypto.mjs'

// 后端：http://nodejs.cn/api/webcrypto.html#class-crypto
// 前端：https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto

const cipher = new Cipher()

const contentTypeMap = {
    hqx: 'application/mac-binhex40',
    cpt: 'application/mac-compactpro',
    csv: ['text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel'],
    bin: 'application/macbinary',
    dms: 'application/octet-stream',
    lha: 'application/octet-stream',
    lzh: 'application/octet-stream',
    exe: ['application/octet-stream', 'application/x-msdownload'],
    class: 'application/octet-stream',
    psd: 'application/x-photoshop',
    so: 'application/octet-stream',
    sea: 'application/octet-stream',
    dll: 'application/octet-stream',
    oda: 'application/oda',
    pdf: ['application/pdf', 'application/x-download'],
    ai: 'application/postscript',
    eps: 'application/postscript',
    ps: 'application/postscript',
    smi: 'application/smil',
    smil: 'application/smil',
    mif: 'application/vnd.mif',
    xls: ['application/excel', 'application/vnd.ms-excel', 'application/msexcel'],
    ppt: ['application/powerpoint', 'application/vnd.ms-powerpoint'],
    wbxml: 'application/wbxml',
    wmlc: 'application/wmlc',
    dcr: 'application/x-director',
    dir: 'application/x-director',
    dxr: 'application/x-director',
    dvi: 'application/x-dvi',
    gtar: 'application/x-gtar',
    gz: 'application/x-gzip',
    php: 'application/x-httpd-php',
    php4: 'application/x-httpd-php',
    php3: 'application/x-httpd-php',
    phtml: 'application/x-httpd-php',
    phps: 'application/x-httpd-php-source',
    js: 'application/x-javascript',
    mjs: 'application/x-javascript',
    swf: 'application/x-shockwave-flash',
    sit: 'application/x-stuffit',
    tar: 'application/x-tar',
    tgz: ['application/x-tar', 'application/x-gzip-compressed'],
    xhtml: 'application/xhtml+xml',
    xht: 'application/xhtml+xml',
    zip: ['application/x-zip', 'application/zip', 'application/x-zip-compressed'],
    mid: 'audio/midi',
    midi: 'audio/midi',
    mpga: 'audio/mpeg',
    mp2: 'audio/mpeg',
    mp3: ['audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'],
    aif: 'audio/x-aiff',
    aiff: 'audio/x-aiff',
    aifc: 'audio/x-aiff',
    ram: 'audio/x-pn-realaudio',
    rm: 'audio/x-pn-realaudio',
    rpm: 'audio/x-pn-realaudio-plugin',
    ra: 'audio/x-realaudio',
    rv: 'video/vnd.rn-realvideo',
    wav: ['audio/x-wav', 'audio/wave', 'audio/wav'],
    bmp: ['image/bmp', 'image/x-windows-bmp'],
    gif: 'image/gif',
    jpeg: ['image/jpeg', 'image/pjpeg'],
    jpg: ['image/jpeg', 'image/pjpeg'],
    jpe: ['image/jpeg', 'image/pjpeg'],
    png: ['image/png', 'image/x-png'],
    tiff: 'image/tiff',
    tif: 'image/tiff',
    css: 'text/css',
    html: 'text/html',
    htm: 'text/html',
    shtml: 'text/html',
    vue: 'text/html',
    txt: 'text/plain',
    text: 'text/plain',
    log: ['text/plain', 'text/x-log'],
    rtx: 'text/richtext',
    rtf: 'text/rtf',
    xml: 'text/xml',
    xsl: 'text/xml',
    mpeg: 'video/mpeg',
    mpg: 'video/mpeg',
    mpe: 'video/mpeg',
    qt: 'video/quicktime',
    mov: 'video/quicktime',
    avi: 'video/x-msvideo',
    movie: 'video/x-sgi-movie',
    doc: 'application/msword',
    docx: ['application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip'],
    xlsx: ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip'],
    word: ['application/msword', 'application/octet-stream'],
    xl: 'application/excel',
    eml: 'message/rfc822',
    json: ['application/json', 'text/json'],
};

// if (existsSync(''))

function server(request, response) {

    let {pathname} = new URL(`https://${request.headers.host}${request.url}`);

    response.setHeader('Access-Control-Allow-Origin', 'http://10.10.3.24:8989');
    response.setHeader('Access-Control-Allow-Headers', '*');

    if (pathname === '/test') {

        let chunks = Buffer.alloc(0)

        request.on('data', chunk => chunks = Buffer.concat([chunks, chunk]))

        request.on('end', () => {

            cipher.decrypt(chunks)
                .then(([iv, key, result]) => cipher.encrypt(key, iv, result))
                .then(buffer => {
                    response.end(buffer)
                })
        })
        return
    }

    if (!pathname.includes('.', pathname.lastIndexOf('/'))) {

        pathname += pathname[pathname.length - 1] === '/' ? 'index.html' : '/index.html';
    }

    pathname = '.' + pathname;

    if (existsSync(pathname)) {

        let type = pathname.substring(pathname.lastIndexOf('.') + 1);

        if (Reflect.has(contentTypeMap, type)) {

            response.setHeader('Content-Type', contentTypeMap[type]);
        }

        createReadStream(pathname).pipe(response);

    } else {

        response.writeHead(404, {'Content-Type': contentTypeMap.html});
        response.end('<meta charset="UTF-8"><title>404</title><div style="text-align: center; padding-top: 4em"><i style="font-size: 8em">404</i>… (っ °Д °)っ</div>');
    }
}

const port = 8088;

createServer(server).listen(port, function (error) {

    if (error) {

        console.error(error);

    } else {

        let interfaces = os.networkInterfaces();

        for (let [key, value] of Object.entries(interfaces)) {

            for (let info of value) {

                if (info.family === 'IPv4' && info.address !== '127.0.0.1' && !info.internal) {

                    console.log(key, info.address);

                    console.log(`HTTP Server is running on: http://${info.address}:${port}`);
                }
            }
        }
    }
});
