import {webcrypto} from 'crypto'

import {existsSync, readFileSync, writeFileSync} from 'fs'

class Cipher {

    #algorithm = {
        name: "AES-CBC",
        iv: undefined
    }
    #key
    #name = 'RSA-OAEP'
    #encoder
    #decoder

    constructor() {

        this.#encoder = new TextEncoder()
        this.#decoder = new TextDecoder()

        if (existsSync('./private.u8')) {

            webcrypto.subtle.importKey(
                "pkcs8", readFileSync('./private.u8'),
                {name: this.#name, hash: "SHA-256"},
                true, ["decrypt"]
            ).then(key => this.#key = key)

        } else {

            webcrypto.subtle.generateKey(
                {
                    name: this.#name,
                    modulusLength: 2048,
                    publicExponent: new Uint8Array([1, 0, 1]),
                    hash: "SHA-256",
                },
                true,
                [
                    "encrypt",
                    "decrypt"
                ]
            ).then((keyPair) => {

                webcrypto.subtle.exportKey(
                    "spki",
                    keyPair.publicKey
                ).then(exported => {

                    writeFileSync('./public.u8', new Uint8Array(exported))
                })

                webcrypto.subtle.exportKey(
                    "pkcs8",
                    keyPair.privateKey
                ).then(exported => {

                    writeFileSync('./private.u8', new Uint8Array(exported))
                })
            })
        }
    }

    encrypt(key, iv, content) {

        content = this.#encoder.encode(content)

        return new Promise(resolve => {

            const pool = []

            this.#algorithm.iv = iv

            for (let i = 0; i < content.length; i += 16) {

                pool.push(webcrypto.subtle.encrypt(
                    this.#algorithm,
                    key,
                    content.subarray(i, i + 16)
                ))
            }

            Promise.all(pool).then(chunks => {

                resolve(Buffer.concat(chunks.map(chunk => new Uint8Array(chunk))))
            })
        })
    }

    decrypt(chunks) {

        const head = chunks.subarray(0, 256)
        const body = chunks.subarray(256)

        const result = []

        return webcrypto.subtle.decrypt(
            {
                name: this.#name
            },
            this.#key,
            head
        ).then(buffer => {
            buffer = new Uint8Array(buffer)
            this.#algorithm.iv = result[0] = buffer.subarray(0, 16)
            return webcrypto.subtle.importKey(
                "raw",
                buffer.subarray(16),
                "AES-CBC",
                true,
                ["encrypt", "decrypt"]
            )
        }).then(key => {
            result[1] = key
            const pool = []
            for (let i = 0; i < body.length; i += 32) {
                pool.push(webcrypto.subtle.decrypt(
                    this.#algorithm,
                    key,
                    body.subarray(i, i + 32)
                ))
            }
            return Promise.all(pool)
        }).then(chunks => {
            chunks = chunks.map(chunk => new Uint8Array(chunk))
            chunks = this.#decoder.decode(Buffer.concat(chunks))
            return Promise.resolve([...result, chunks])
        })
    }
}

export {
    Cipher
}
